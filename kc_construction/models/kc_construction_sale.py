from openerp import models, fields, api, _
from openerp.exceptions import UserError
import time

class res_partner(models.Model):
	_inherit = 'res.partner'

	tax_registration_no = fields.Char('Tax Registration Number')
	company_id = fields.Many2one('res.company','Company')


class partner_agent(models.Model):
	_name = 'partner.agent'

	name = fields.Char('Agent Name')
	code = fields.Char('Code')
	agent_type = fields.Selection([('adviser','Adviser'),('commercial','Commercial')], 'Type')
	partner_id = fields.Many2one('res.partner','Customer')
	retantion_id = fields.Many2one('account.tax', 'Applied Retantion')
	active = fields.Boolean('Active')
	settlement = fields.Selection([('month','Monthly'),('quarter','Quartely'),('semiannual','Semi Ammual'),('annual','Annual')], 'Time Settlement')
	commission_percentage = fields.Float('Commission Percentage')
	coomission_amt_per_item = fields.Float('Commission Amount Per Item')
	agent_id = fields.Many2one('sale.order', 'Agent')


class sale_order(models.Model):
	_inherit = 'sale.order'

	sale_agent_ids = fields.One2many('partner.agent', 'agent_id', 'Agents')
	new_project = fields.Boolean('Create New Project')
	new_start_date = fields.Datetime('Expected Start Date')
	new_end_date = fields.Datetime('Expected End Date')
	project_id = fields.Many2one('project.project', 'Project')


class product_template(models.Model):
	_inherit = 'product.template'

	company_id = fields.Many2one('res.company', 'Company')