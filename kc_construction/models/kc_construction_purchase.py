from openerp import models, fields, api, _
from openerp.exceptions import UserError
import time


class purchase_order(models.Model):
	_inherit = 'purchase.order'

	purchase_project_id = fields.Many2one('project.project', 'Project')
	company_id = fields.Many2one('res.company', 'Company')
	purchase_order_history_ids = fields.One2many('purchase.order.history', 'history_id', 'Purchase Order History')


class purchase_order_history(models.Model):
	_name = 'purchase.order.history'

	history_id = fields.Many2one('purchase.order', 'History')
	history_date = fields.Datetime('History Date')
	description = fields.Char('Description')
