from openerp import models, fields, api, _
from openerp.exceptions import UserError
import time

class project(models.Model):
	_inherit = 'project.project'

	@api.depends('work_package_ids')
	def cost_project(self):
		price = 0.0
		for line in self.work_package_ids:
			price = line.package_total + price
			self.update({
				'project_cost' : price,
			})

	project_cost = fields.Float(compute='cost_project', string='Project Cost', store=True)
	markup_cost = fields.Float('Markup Cost')
	estimated_cost = fields.Float('Estimated Cost')
	team_members = fields.Many2many('res.users', 'project_users_members', 'project_member_id', 'user_member_id', 'Members')
	project_start_date = fields.Datetime('Start Date')
	project_end_date = fields.Datetime('End Date')
	project_parent_id = fields.Many2one('project.project', 'Parent Project')
	sale_order_ids = fields.One2many('sale.order', 'project_sale_id', 'Sale Orders')
	purchase_order_ids = fields.One2many('purchase.order', 'project_purchase_id', 'Purchase Orders')
	project_task_ids = fields.One2many('project.task', 'project_task_id', 'Tasks')
	stage_type_ids = fields.Many2many('project.task.type', 'project_stage_type', 'project_type_id', 'stage_type_id', 'Project Stages')
	work_package_ids = fields.Many2many('raw.material.work.package', 'project_work_package', 'project_package_id', 'work_package_id', 'Work Package')


class sale_order(models.Model):
	_inherit = 'sale.order'

	project_sale_id = fields.Many2one('project.project', 'Projects')


class purchase_order(models.Model):
	_inherit = 'purchase.order'

	project_purchase_id = fields.Many2one('project.project', 'Projects')


class task(models.Model):
	_inherit = 'project.task'

	project_task_id = fields.Many2one('project.project', 'Projects')


class raw_material_cost(models.Model):
	_name = 'raw.material.cost'

	@api.depends('unit_price', 'quantity')
	def amount_raw_material(self):
		price = self.unit_price * self.quantity
		self.update({
			'subtotal' : price,
		})

	code = fields.Integer('Raw Material Number')
	name = fields.Char('Raw Material Name')
	description = fields.Text('Description')
	unit_price = fields.Float('Unit Price')
	quantity = fields.Integer('Quantity')
	subtotal = fields.Float(compute='amount_raw_material', string='Subtotal', store=True)
	product_uom = fields.Many2one('product.uom', 'Unit of Measure')


class raw_material_header(models.Model):
	_name = 'raw.material.header'

	@api.depends('raw_material_cost_ids')
	def cost_header(self):
		price = 0.0
		for line in self.raw_material_cost_ids:
			price = line.subtotal + price
			self.update({
				'cost_of_raw_material' : price,
			})

	code = fields.Integer('Raw Material Header Number')
	name = fields.Char('Raw Material Header Name')
	cost_of_raw_material = fields.Float(compute='cost_header', string='Cost of Header', store=True)
	raw_material_cost_ids = fields.Many2many('raw.material.cost', 'raw_material_cost_header', 'cost_id', 'header_id', 'Raw Materials Cost') 


class raw_material_work_package(models.Model):
	_name = 'raw.material.work.package'

	@api.depends('raw_material_header_ids')
	def cost_work_package(self):
		price = 0.0
		for line in self.raw_material_header_ids:
			price = line.cost_of_raw_material + price
			total = price * self.work_package_qty
			self.update({
				'cost_of_work_package' : price,
				'package_total' : total,
			})

	name = fields.Char('Raw Material Work Package Name')
	cost_of_work_package = fields.Float(compute='cost_work_package', string='Cost of Work Package', store=True)
	raw_material_header_ids = fields.Many2many('raw.material.header', 'raw_material_header_package', 'header_id', 'package_id', 'Raw Materials Headers') 
	work_package_qty = fields.Integer('Quantity')
	package_total = fields.Float(compute='cost_work_package', string='Total', store=True)


class project_user_allocation(models.Model):
	_name = 'project.user.allocation'

	user_id = fields.Many2one('res.users', 'User')
	project_id = fields.Many2one('project.project', 'Project')
	phase_id = fields.Many2one('project.phase', 'Project Phase')
	date_start = fields.Datetime('Start Date')
	date_end = fields.Datetime('End Date')
	project_allocate_id = fields.Many2one('project.phase', 'Project Phase')


class proejct_phase(models.Model):
	_name = 'project.phase'

	name = fields.Char('Name')
	phase_start_date = fields.Datetime('Start Date')
	phase_end_date = fields.Datetime('End Date')
	project_id = fields.Many2one('project.project')
	user_ids = fields.One2many('project.user.allocation', 'project_allocate_id', 'User Allocation')
	phase_task_ids = fields.One2many('project.task', 'project_task_id', 'Tasks')
	min_start_date = fields.Datetime('Minimum Start Date')
	deadline_date = fields.Datetime('Deadline Date')
	user_force_ids = fields.Many2many('res.users', 'project_phase_user', 'project_phase_id', 'user_id_phase', 'Force Assigned Users')
	sequence = fields.Integer('Sequence')


class bill_of_qty(models.Model):
	_name = 'bill.of.qty'

	project_id = fields.Many2one('project.project', 'Project')
	material_cost = fields.Float('Material Cost')
	equipment_cost = fields.Float('Equipment Cost')
	labor_cost = fields.Float('Labor Cost')
	estimated_cost = fields.Float('Estimated Cost')
	revision = fields.Integer('Revision')
	bill_qty_line_ids = fields.One2many('bill.of.qty.line', 'line_id', 'Bill Of Quantity Line')


class bill_of_qty_line(models.Model):
	_name = 'bill.of.qty.line'

	@api.depends('line_amount', 'line_qty')
	def line_total_amount(self):
		price = self.line_amount * self.line_qty
		self.update({
			'line_total' : price,
		})

	line_id = fields.Many2one('bill.of.qty', 'Line Id')
	line_type = fields.Reference(selection=[('raw.material.cost', 'Material'), ('hr.equipment', 'Equipment'), ('hr.employee', 'Labor')], string="Type", size=32)
	line_qty = fields.Integer('Qty')
	line_uom = fields.Many2one('product.uom', 'Unit')
	line_amount = fields.Float('Rate')
	line_total = fields.Float(compute='line_total_amount', string='Subtotal', store=True)