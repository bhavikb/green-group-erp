from openerp import models, fields, api, _
from openerp.exceptions import UserError
from datetime import datetime, timedelta

class hr_holidays(models.Model):

    _inherit = 'hr.holidays'

    def create(self, cr, uid, vals, context=None):
		res = super(hr_holidays, self).create(cr, uid, vals, context=context)
		print "ressttttttttttttttttssssssss", res
		self.create_leave_request_mail(cr, uid, [res], context=context)
		return res

    def create_leave_request_mail(self, cr, uid, ids, context=None):
        mail_ids = []
        mail_pool = self.pool['mail.mail']
        template_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'kc_refuse_leave', 'leave_request_email_template')[1]
        email_obj = self.pool.get('mail.template')
        print "create_leaveeeeeeeeeeeeeee_request_mail", template_id
        mail_id = email_obj.send_mail(cr, uid, template_id, ids[0], context=context)
        mail_ids.append(mail_id)

        if mail_ids:
            res = mail_pool.send(cr, uid, mail_ids, context=context)

        return res
    
    def holidays_validate(self, cr, uid, ids, context=None):
        mail_ids = []
        mail_pool = self.pool['mail.mail']
        template_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'kc_refuse_leave', 'leave_approve_template')[1]
        email_obj = self.pool.get('mail.template')
        print "hollllllllllllllllllllidaysssss vvvvvvvvvvvvvvalidate", template_id
        mail_id = email_obj.send_mail(cr, uid, template_id, ids[0], context=context)
        mail_ids.append(mail_id)

        if mail_ids:
            res = mail_pool.send(cr, uid, mail_ids, context=context)

        return super(hr_holidays, self).holidays_validate(cr, uid, ids, context=context)
    
    def holidays_refuse(self, cr, uid, ids, context=None):
        mail_ids = []
        mail_pool = self.pool['mail.mail']
        template_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'kc_refuse_leave', 'leave_refuse_template')[1]
        email_obj = self.pool.get('mail.template')
        print "hollllllllllllllllllllidaysssss reffffffffffffff", template_id
        mail_id = email_obj.send_mail(cr, uid, template_id, ids[0], context=context)
        mail_ids.append(mail_id)

        if mail_ids:
            res = mail_pool.send(cr, uid, mail_ids, context=context)

        return super(hr_holidays, self).holidays_refuse(cr, uid, ids, context=context)