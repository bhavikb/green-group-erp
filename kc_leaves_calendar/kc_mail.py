from openerp import models, fields, api, _
from openerp.exceptions import UserError
from datetime import datetime, timedelta
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT

class SentMail(models.Model):
    _inherit = "mail.compose.message"
    _rec_name = 'subject'

    is_sent = fields.Boolean('Mail Sent')

    @api.one
    def send_mail_action(self):
        res = super(SentMail, self).send_mail_action()
        self.is_sent =  True
        # res = self.env['ir.actions.act_window'].for_xml_id('kc_leaves_calendar', 'action_inbox_mail')
        return res

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        context = self._context or {}
        if context.get('my'):
            partner_id = self.env['res.users'].browse(self._uid).partner_id.id
            args += [('partner_ids', 'in', [partner_id])]
        return super(SentMail, self).search(args, offset, limit, order, count=count)

class SearchMail(models.Model):
    _inherit = "mail.message"
    _rec_name = 'subject'

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        context = self._context or {}
        if context.get('myinbox'):
            partner_id = self.env['res.users'].browse(self._uid).partner_id.id
            args += [('partner_ids', 'in', [partner_id])]
        return super(SearchMail, self).search(args, offset, limit, order, count=count)
        