from openerp import models, fields, api, _
from openerp.exceptions import UserError
from datetime import datetime, timedelta
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT

# -------------- Holidays & Meeting ---------------
class event(models.Model):
    _inherit = "calendar.event"

    is_leave = fields.Boolean('Allow to Leave')
    # start_date = fields.Datetime('Start Date', states={'done': [('readonly', True)]}, track_visibility='onchange')
    start_date = fields.Datetime('Start Date')
    stop_date = fields.Datetime('End Date')

    @api.multi
    @api.depends('name')
    def name_get(self):
        result =[] 
        for cal in self:
            if cal.is_leave:
                result.append((cal.id, 'On Leave'))
            else:
                result.append((cal.id, 'In A Meeting'))
        return result

class holidays(models.Model):
    _inherit = "hr.holidays"
    
    manager_id = fields.Many2one('hr.employee', related='employee_id.parent_id', string='Manager', store=False)

    @api.one
    def holidays_validate(self):
        if self.manager_id.user_id.id != self._uid:
            if not self.user_has_groups('kc_leaves_calendar.is_admin'):
                raise UserError(_('You cannot Approve leave of this employee.'))
        res = super(holidays, self).holidays_validate()
        self.meeting_id.is_leave =  True
        return res

    @api.one
    def holidays_refuse(self):
        if self.manager_id.user_id.id != self._uid:
            if not self.user_has_groups('kc_leaves_calendar.is_admin'):
                raise UserError(_('You cannot Approve leave of this employee.'))
        return super(holidays, self).holidays_refuse()

    @api.multi    
    def _get_default_status(self):
        res = self.env['hr.holidays.status'].search([('name','=','Unpaid')])
        return res and res[0] or False

    holiday_status_id = fields.Many2one('hr.holidays.status', string='Leave Type', required=False, default=_get_default_status)
    