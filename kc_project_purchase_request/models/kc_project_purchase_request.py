# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Key Concepts IT Services LLP.
#    (<http://keyconcepts.co.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from openerp import api, fields, models, _
from openerp.exceptions import UserError


class ProjectPurchaseRequest(models.Model):
    _name = 'project.purchase.request'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _rec_name = 'name'

    name = fields.Char(string="Name", default='New', copy=False, track_visibility='onchange')
    project_id = fields.Many2one('project.project', string='Project', required=True)
    note = fields.Text(string="Note")
    request_state = fields.Selection([
            ('draft', 'Draft'),
            ('order', 'Order'),
            ('done', 'Done'),
            ('cancel', 'Cancel'),
        ], string='Status', default='draft', track_visibility='onchange')
    request_date = fields.Date(string="Request Date", required=True, default=fields.date.today(), track_visibility='onchange')
    shedueld_date = fields.Date(string="Shedueld Date", track_visibility='onchange')
    purchase_id = fields.Many2one('purchase.order', 'Purchase Order', track_visibility='onchange', domain=[('state', 'in', ['purchase', 'done', 'cancel'])])
    agency_id = fields.Many2one('res.users', string="Purchase Requester", required=True)
    product_id = fields.Many2one('product.product', string="Materials", required=True, track_visibility='onchange', domain=[('purchase_ok', '=', True)])
    product_qty = fields.Float(string="Request Qty", required=True, track_visibility='onchange')
    product_uom = fields.Many2one(string="UOM", related='product_id.uom_id', required=True, track_visibility='onchange')

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('project.purchase.request') or 'New'
        result = super(ProjectPurchaseRequest, self).create(vals)
        if result['product_qty'] <= 0:
            raise UserError(_('Please Add materials Qty more than 0'))
        if result['purchase_id']:
            result['request_state'] = 'order'
        return result

    @api.multi
    def pr_order(self):
        self.request_state = 'order'

    @api.multi
    def pr_done(self):
        self.request_state = 'done'

    @api.multi
    def pr_cancel(self):
        self.request_state = 'cancel'

    @api.multi
    def pr_draft(self):
        self.request_state = 'draft'
