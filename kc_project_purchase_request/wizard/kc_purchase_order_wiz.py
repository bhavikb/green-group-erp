# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Key Concepts IT Services LLP.
#    (<http://keyconcepts.co.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from openerp import api, fields, models


class purchaseOrderWiz(models.TransientModel):
    _name = 'kc.purchase.order.wiz'

    purchase_id = fields.Many2one('purchase.order', 'Purchase Order', required=True, domain=[('state', 'in', ['purchase', 'done', 'cancel'])])
    scheduled_date = fields.Date(string="Scheduled Date")

    @api.multi
    def set_po(self):
        if self._context.get('active_model') == 'project.purchase.request':
            ppr_object = self.env['project.purchase.request'].browse(self._context.get('active_ids'))
            for ppr in ppr_object:
                if ppr.request_state == 'draft':
                    if not ppr.purchase_id and ppr.request_state not in ['cancel', 'order']:
                        ppr.purchase_id = self.purchase_id
                        ppr.request_state = 'order'
                        if self.scheduled_date:
                            ppr.shedueld_date = self.scheduled_date
