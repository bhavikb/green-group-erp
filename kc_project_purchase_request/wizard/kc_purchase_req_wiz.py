# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Key Concepts IT Services LLP.
#    (<http://keyconcepts.co.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from openerp import api, fields, models, _
from openerp.exceptions import UserError


class ProjectPurchaseRequestWiz(models.TransientModel):

    """ Project purchase request """
    _name = 'project.purchase.request.wiz'

    project_id = fields.Many2one('project.project', string='Project', required=True)
    agency_id = fields.Many2one('res.users', string="Purchase Requester", required=True)
    order_line = fields.One2many('project.purchase.request.line.wiz', 'order_id', string='Order Lines')
    request_date = fields.Date(string="Request Date", required=True, default=fields.date.today())
    shedueld_date = fields.Date(string="Shedueld Date")
    note = fields.Text(string="Note")

    @api.model
    def default_get(self, fields):
        rec = super(ProjectPurchaseRequestWiz, self).default_get(fields)
        if self._context.get('active_model') == 'project.project':
            project_id = self.env['project.project'].browse(self._context.get('active_id'))
            rec['project_id'] = project_id.id
        return rec

    @api.multi
    def project_purchase(self):
        for line in self.order_line:
            if line.product_qty <= 0:
                raise UserError(_("Please Add materials Qty more than 0"))

        for line in self.order_line:
            if line:
                request_data = {
                    'agency_id': self.agency_id.id,
                    'project_id': self.project_id.id,
                    'request_date': self.request_date,
                    'shedueld_date': self.shedueld_date,
                    'note': self.note,
                    'product_id': line.product_id.id,
                    'product_qty': line.product_qty,
                    'product_uom': line.product_uom,
                    'request_state': 'draft',

                }
                print self.env['project.purchase.request'].create(request_data)
        return True


class ProjectPurchaseRequestLineWiz(models.TransientModel):

    """ Project purchase request """
    _name = 'project.purchase.request.line.wiz'

    product_id = fields.Many2one('product.product', string="Materials", required=True, domain=[('purchase_ok', '=', True)])
    product_qty = fields.Float(string="Request Qty", required=True, default="1")
    product_uom = fields.Many2one(string="UOM", related='product_id.uom_id', required=True)
    order_id = fields.Many2one('project.purchase.request.wiz', string='Order Reference', required=True, ondelete='cascade', index=True, copy=False)
