# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Key Concepts IT Services LLP.
#    (<http://keyconcepts.co.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from openerp import api, fields, models, _
from openerp.exceptions import UserError


class WareHouseProject(models.Model):
    _inherit = 'stock.warehouse'

    project_id = fields.Many2one('project.project', string='Project', required=True, track_visibility='onchange')
    project_manager = fields.Many2one(string="Manager", related='project_id.user_id', track_visibility='onchange')

    @api.multi
    def unlink(self):
        warehouse_obj = self.env['stock.move'].browse(['|', ('location_id.warehouse_id', '=', self.id), ('location_dest_id.warehouse_id', '=', self.id)])
        if warehouse_obj:
            raise UserError(_('First remove the Move. Then after Delete Warehouse'))
        return super(WareHouseProject, self).unlink()


class ProjectWarehouse(models.Model):
    _inherit = 'project.project'

    pr_warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', track_visibility='onchange')

    @api.model
    def create(self, vals):
        result = super(ProjectWarehouse, self).create(vals)
        data = {
            'name': result['name'],
            'code': result['name'],
            'partner_id': result['user_id'].id,
            'project_id': result.id,
        }
        wh = self.env['stock.warehouse'].create(data)
        loc_data = {
            'name': result['name'],
            'location_id': stock_location_locations_virtual,
            'active': True,
            'usage': 'production',
        }
        self.env['stock.location'].create(loc_data)
        result['pr_warehouse_id'] = wh.id
        return result

    @api.multi
    def unlink(self):
        warehouse_obj = self.env['stock.warehouse'].search([('project_id', '=', self.id)])
        if warehouse_obj:
            raise UserError(_('First remove the warehouse. Then after Delete Project'))
        return super(ProjectWarehouse, self).unlink()
