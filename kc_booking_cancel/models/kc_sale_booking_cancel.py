# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Key Concepts IT Services LLP.
#    (<http://keyconcepts.co.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from openerp import api, models


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.v8
    @api.returns('self')
    def refund(self, date_invoice=None, date=None, description=None, journal_id=None):
        res = super(AccountInvoice, self).refund()
        return res


class SaleOrderCancel(models.Model):
    _inherit = "sale.order"

    @api.multi
    def action_cancel(self):
        res = super(SaleOrderCancel, self).action_cancel()
        project_task = self.env['project.task'].search([('sale_line_id.order_id', '=', self.id)])
        if project_task:
            for task in project_task:
                task.active = False
        for line in self.order_line:
            if line.product_id.type == 'service' and line.product_id.track_service == 'task':
                line.product_id.active = True

        account_invoice = self.env['account.invoice'].search([('origin', '=', self.name)])
        for invoice in account_invoice:
            if invoice.state == 'draft':
                invoice.state = 'cancel'
            elif invoice.state == 'paid':
                invoice.refund()
            elif invoice.state == 'open':
                invoice.refund()
        return res
