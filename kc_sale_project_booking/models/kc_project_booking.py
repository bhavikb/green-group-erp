# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import api, fields, models

class Project(models.Model):
    _inherit = "project.project"

    @api.model
    def _task_count(self):
        for project in self:
            project.task_count = self.env['project.task'].search_count([('project_id', '=', project.id), ('is_booking', '=', False)])

    @api.model
    def _issue_count(self):
        for issue in self:
            issue.issue_count = self.env['project.issue'].search_count([('project_id', '=', issue.id), ('is_booking_issue', '=', False)])

    task_count = fields.Integer(compute='_task_count', string="Tasks")
    issue_count = fields.Integer(compute='_issue_count', string="Task Issues",)
    label_issues = fields.Char(string="Use Task Issue as", default=" Task Issues")


class AccountAnalyticLine(models.Model):
    _inherit = "account.analytic.line"

    task_id = fields.Many2one('project.task', string='Bookvcbnvnjkmgk', domain=[('is_booking', '=', False)])

class AccountAnalyticAccount(models.Model):
    _inherit = 'account.analytic.account'

    use_issues = fields.Boolean(string='Tasks Issues', default=True)
