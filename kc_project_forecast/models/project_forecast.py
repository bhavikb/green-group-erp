# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Key Concepts IT Services LLP.
#    (<http://keyconcepts.co.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from openerp import fields, models, api, _
from openerp.exceptions import UserError

class ProjectTask(models.Model):
    _inherit = 'project.task'

    date_start = fields.Date(string="Start Date")

    @api.model
    def create(self, vals):
        task_id = super(ProjectTask, self).create(vals)
        data = {
            'project_id': task_id.project_id.id,
            'task_id': task_id.id,
            'start_date': task_id.date_start,
            'end_date': task_id.date_deadline,
            }
        self.env['project.forecast'].create(data)
        return task_id

    @api.multi
    def write(self, vals):
        result = super(ProjectTask, self).write(vals) 
        forecast_id = self.env['project.forecast'].search([('task_id', '=', self.id)])
        num_forecast = len(forecast_id.ids)
        if 'date_start' in vals:
            if num_forecast > 1:
                raise UserError(_("More then one Forcast create for same task, please change Start date from forcast."))
            forecast_id.start_date = self.date_start
        if 'date_deadline' in vals:
            if num_forecast > 1:
                raise UserError(_("More then one Forcast create for same task, please change deadline from forcast."))
            forecast_id.end_date = self.date_deadline                    
        return result