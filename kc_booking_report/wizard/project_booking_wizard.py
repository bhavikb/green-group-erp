# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Key Concepts IT Services LLP.
#    (<http://keyconcepts.co.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################
from openerp import models, fields, api, _
from openerp.exceptions import UserError
from datetime import date, timedelta, datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
import tempfile
import xlwt
from xlwt import Workbook
from cStringIO import StringIO
import base64
import time
from dateutil.relativedelta import relativedelta
from pytz import timezone
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT,misc,DEFAULT_SERVER_DATETIME_FORMAT

class ProjectBookingExcelExtended(models.Model):
    _name= "project.booking.excel.extended"

    excel_file = fields.Binary('Download Report :- ')
    file_name = fields.Char('Excel File', size=64)


class BookingWiz(models.TransientModel):
    _name = 'booking.date.wise.report'

    project_id = fields.Many2one('project.project', 'Project')
    start_date = fields.Date('Start Date', default=fields.Datetime.now(), required=True)
    end_date = fields.Date('End Date', default=fields.Datetime.now(), required=True)
    report_by_project = fields.Selection([('all', 'All'), ('project', 'Project')], default='all', string="Report By Project")
    stage_id = fields.Many2one('project.task.type', 'Stage')

    @api.onchange('project_id')
    def onchange_project_id(self):
        stage_ids = self.env['project.task.type'].search([('project_ids', 'in', self.project_id.id)]).ids
        return {'domain' : {'stage_id': [('id', 'in', stage_ids)]}}


    @api.multi
    def date_wise_flat_and_bulding_report(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['start_date', 'end_date'])[0]
        if self.report_by_project == 'project':
            booking_search_obj = self.env['project.task'].search([('create_date', '>=', self.start_date), ('create_date', '<=', self.end_date), ('stage_id.id', '=', self.stage_id.id), ('project_id', '=', self.project_id.id)]).ids
        
        elif self.report_by_project == 'all':
            booking_search_obj = self.env['project.task'].search([('create_date', '>=', self.start_date), ('create_date', '<=', self.end_date)]).ids
        datas = {
            'ids': booking_search_obj,
            'model': 'project.task',
            'form': booking_search_obj,
            }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'kc_booking_report.report_project_booking',
            'datas': datas}

    @api.multi
    def date_wise_project_booking_excel_report(self):
        date = fields.date.today().strftime('%d-%m-%Y')
        filename= 'Project Booking_' + str(date) + '.xls'
        workbook= xlwt.Workbook(encoding="UTF-8")

        worksheet= workbook.add_sheet('Project Booking')
        font = xlwt.Font()
        font.bold = True
        for_left = xlwt.easyxf("font: bold 1, color black; borders: top double, bottom double, left double, right double; align: horiz left")
        for_left_not_bold = xlwt.easyxf("font: color black; align: horiz left")
        for_center_bold = xlwt.easyxf("font: bold 1, color black; align: horiz center")
        GREEN_TABLE_HEADER = xlwt.easyxf(
                 'font: bold 1, name Tahoma, height 250;'
                 'align: vertical center, horizontal center, wrap on;'
                 'borders: top double, bottom double, left double, right double;'
                 )
        style = xlwt.easyxf('font:height 400, bold True, name Arial; align: horiz center, vert center;borders: top medium,right medium,bottom medium,left medium')
        
        alignment = xlwt.Alignment()  # Create Alignment
        alignment.horz = xlwt.Alignment.HORZ_RIGHT
        style = xlwt.easyxf('align: wrap yes')
        style.num_format_str = '0.00'
        
        worksheet.row(0).height = 500
        worksheet.col(0).width = 5000
        worksheet.col(1).width = 10000
        worksheet.col(2).width = 4000
        worksheet.col(3).width = 5000
        worksheet.col(4).width = 4000
        worksheet.col(5).width = 4000
        worksheet.col(6).width = 7000
       
        borders = xlwt.Borders()
        borders.bottom = xlwt.Borders.MEDIUM
        border_style = xlwt.XFStyle()
        border_style.borders = borders

        worksheet.write_merge(0,0,0,7,'Project Booking Report',GREEN_TABLE_HEADER)

        if self.report_by_project == 'project':
            booking_search_ids = self.env['project.task'].search([('create_date', '>=', self.start_date), ('create_date', '<=', self.end_date), ('stage_id.id', '=', self.stage_id.id), ('project_id', '=', self.project_id.id)])
        
        elif self.report_by_project == 'all':
            booking_search_ids = self.env['project.task'].search([('create_date', '>=', self.start_date), ('create_date', '<=', self.end_date)])
        row = 1

        worksheet.write(row, 0, 'Booking Date' or '',for_left)
        worksheet.write(row, 1, 'Booking Name' or '',for_left)
        worksheet.write(row, 2, 'Project Name' or '',for_left)
        worksheet.write(row, 3, 'Project Manager' or '',for_left)
        worksheet.write(row, 4, 'Customer Name' or '',for_left)
        worksheet.write(row, 5, 'Contect' or '',for_left)
        worksheet.write(row, 6, 'Email' or '',for_left)

        for res in booking_search_ids:
            row = row + 1
            worksheet.write(row, 0, res.create_date or '',for_left_not_bold)
            worksheet.write(row, 1, res.name or '',for_left_not_bold)
            worksheet.write(row, 2, res.project_id.name or '',for_left_not_bold)
            worksheet.write(row, 3, res.user_id.name or '',for_left_not_bold)
            worksheet.write(row, 4, res.partner_id.name or '',for_left_not_bold)
            worksheet.write(row, 5, res.partner_id.mobile or '',for_left_not_bold)
            worksheet.write(row, 6, res.partner_id.email or '',for_left_not_bold)

        fp = StringIO()
        workbook.save(fp)
        export_id = self.env['project.booking.excel.extended'].create({'excel_file': base64.encodestring(fp.getvalue()), 'file_name': filename})
        fp.close()

        return{
            'view_mode': 'form',
            'res_id': export_id.id,
            'res_model': 'project.booking.excel.extended',
            'view_type': 'form',
            'type': 'ir.actions.act_window',
            'context': self._context,
            'target': 'new',
        }

