# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Key Concepts IT Services LLP.
#    (<http://keyconcepts.co.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from openerp import fields, models, osv, api, _

class CrmSale(models.Model):
    _inherit = 'sale.order'

    product = fields.Many2one('product.product', 'Product')

    @api.model
    def default_get(self, fields):
        rec = super(CrmSale, self).default_get(fields)

        products = self.env['product.product'].search([('id', '=', rec.get('product'))])
        line = {
                'product_id': rec.get('product'),
                'name': products.description_sale or products.display_name,
                'price_unit': products.lst_price,
                'price_unit': products.lst_price,
                'product_uom_qty': 1,
                'product_uom': products.uom_id.id,
                }
        if rec.get('product'):
            rec['order_line'] = [(0, 0, line)]
        return rec

    @api.multi
    def action_confirm(self):
        res = super(CrmSale, self).action_confirm()
        for line in self.order_line:
            if line.product_id.type == 'service' and line.product_id.track_service == 'task':
                line.product_id.active = False
        return res

class ProcurementOrder(models.Model):
    _inherit = "procurement.order"

    @api.model
    def _get_project(self, procurement):
        project = super(ProcurementOrder, self)._get_project(procurement)
        return project

    @api.model
    def _create_service_task(self, procurement):
        task_id = super(ProcurementOrder, self)._create_service_task(procurement)
        task = self.env["project.task"].browse(task_id)
        project = self._get_project(procurement)
        if procurement.sale_line_id.order_id.partner_id.mobile:
            task_name = '%s:%s(%s) - %s' % (procurement.product_id.display_name or '', procurement.sale_line_id.order_id.partner_id.name or '',procurement.sale_line_id.order_id.partner_id.mobile or '', procurement.origin or '')
        else:
            task_name = '%s:%s - %s' % (procurement.product_id.display_name or '', procurement.sale_line_id.order_id.partner_id.name or '', procurement.origin or '')
        task.name= task_name
        task.is_booking = True
        task.user_id = project and project.user_id.id or False
        task.partner_id = procurement.sale_line_id.order_id.partner_id.id or False
        return task_id

class SaleConfiguration(models.TransientModel):
    _inherit = 'sale.config.settings'

    group_product_variant = fields.Selection([
            (0, "No variants on products"),
            (1, "Products can have several attributes, defining variants(Example: size, color,...)")
            ], "Product Variants", default='1', implied_group='product.group_product_variant')
