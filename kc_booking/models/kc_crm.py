# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Key Concepts IT Services LLP.
#    (<http://keyconcepts.co.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from openerp import fields, models, api

class CRMLead(models.Model):
    _inherit = 'crm.lead'

    project_id = fields.Many2one('project.project', 'Project', required=True, attrs="{'invisible': [('state', 'not in', 'close')]}")
    product_id = fields.Many2one('product.product', 'Product', attrs="{'invisible': [('state', 'not in', 'close')]}")
    lead_won = fields.Boolean(string="Lead Won")
    is_user = fields.Boolean(string="User")
    email_from = fields.Char(related="partner_id.email")
    phone = fields.Char(related="partner_id.phone")

    @api.onchange('project_id')
    def onchange_project_id(self):
        product_ids = self.env['product.product'].search([('project_id', '=', self.project_id.id)]).ids
        return {'domain' : {'product_id': [('id', 'in', product_ids)]}}

    @api.one
    def action_set_won(self):
        res = super(CRMLead, self).action_set_won()
        self.lead_won = True
        return res



