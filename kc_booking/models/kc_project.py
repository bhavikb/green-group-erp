# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Key Concepts IT Services LLP.
#    (<http://keyconcepts.co.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from openerp import models, fields, api, _

class Project(models.Model):
    _inherit = 'project.project'

    user_id = fields.Many2one('res.users', string='Project Manager', domain="[('share','=',False)]")
    lable_booking = fields.Char(string="Use Booking as", default=" Booking")
    lable_booking_issue = fields.Char(string="Use Booking Issue as", default=" Booking Issue")


    @api.one
    def _count_lead(self):
        self.count_lead = self.env['crm.lead'].search_count([('project_id', '=', self.id)])

    @api.one
    def _count_booking(self):
        for booking in self:
            booking.count_booking = self.env['project.task'].search_count([('project_id', '=', self.id), ('is_booking', '=', True)])

    @api.one
    def _count_booking_issue(self):
        for booking_issue in self:
            booking_issue.count_booking_issue = self.env['project.issue'].search_count([('project_id', '=', self.id), ('is_booking_issue', '=', True)])

    count_lead = fields.Integer(compute='_count_lead', string='Count Lead')
    count_booking = fields.Integer(compute='_count_booking', string='Count Booking')
    count_booking_issue = fields.Integer(compute='_count_booking_issue', string='Count Booking Issue')


class ProjectIssue(models.Model):
    _inherit = 'project.task'

    @api.one
    def _count_issue(self):
        self.count_issue = self.env['project.issue'].search_count([('task_id', '=', self.id)])

    count_issue = fields.Integer(compute='_count_issue', string='Issue Count')
    user_id = fields.Many2one('res.users', string='Project Manager', select=True, track_visibility='onchange', domain="[('share','=',False)]")
    is_booking = fields.Boolean(string="Is Booking?")

class TaskIssue(models.Model):
    _inherit = 'project.issue'

    user_id = fields.Many2one('res.users', string='Assigned to', select=True, track_visibility='onchange', domain="[('share','=',False)]")
    is_booking_issue = fields.Boolean(string="Is Booking Issue?")
    agency_contect = fields.Many2one('res.partner', string="Agency Contect", track_visibility='onchange')
    agency_email = fields.Char(string='Agency Email', readonly=True, track_visibility='onchange')
    agency_mobile = fields.Char(string='Agency Mobile', readonly=True, track_visibility='onchange')


class AccountAnalyticAccount(models.Model):
    _inherit = 'account.analytic.account'

    use_booking = fields.Boolean(string='Booking', default=True)
    use_booking_issue = fields.Boolean(string='Booking Issue')



