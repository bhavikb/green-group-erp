# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Key Concepts IT Services LLP.
#    (<http://keyconcepts.co.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from openerp import fields, models, api, _
from datetime import datetime, timedelta
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT, ustr

def now(**kwargs):
    dt = datetime.now() + timedelta(**kwargs)
    return dt.strftime(DEFAULT_SERVER_DATETIME_FORMAT)

class ResPartner(models.Model):
    _inherit = 'res.partner'

    is_portal = fields.Boolean(string='Is Portal')

class CrmLeadUser(models.TransientModel):
    _name = 'crm.lead.user'

    partner_id = fields.Many2one('crm.lead', string='Partner')
    user = fields.Char(string='Name', required=True)
    email = fields.Char(string='Email Address', required=True)

    @api.model
    def default_get(self, fields):
        rec = super(CrmLeadUser, self).default_get(fields)
        users = self.env['crm.lead'].browse(self._context.get('active_id'))
        rec['user'] = users.partner_id.name
        rec['email'] = users.partner_id.email
        return rec

    @api.multi
    def create_login_user(self):
        partners = self.env['crm.lead'].browse(self._context.get('active_id'))
        partners.is_user = True
        user_id = self.env['res.users'].with_context({'create_user': True}).create({
            'name': self.user,
            'login': self.email,
            'in_group_11': False,
            'in_group_12': True,
            'in_group_13': True,
            'in_group_63': False,
            'in_group_66': False,
            'sel_groups_4_14_15': '',

            })
        contact_id = self.env['res.partner'].search([('name', '=', self.user)])
        partner = False
        for contact in contact_id:
            if contact.commercial_partner_id.id != user_id.partner_id.id:
                partner = contact.commercial_partner_id
                partner.is_portal=True
            if contact.commercial_partner_id.id == user_id.partner_id.id:
                contact.active = False
        if partner:
            user_id.partner_id = partner

        # no time limit for initial invitation, only for reset password
        expiration = now(days=+1)

        user_id.action_reset_password()