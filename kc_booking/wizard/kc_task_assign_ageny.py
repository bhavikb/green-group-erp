# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Key Concepts IT Services LLP.
#    (<http://keyconcepts.co.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from openerp import fields, models, api, _

class resPartner(models.TransientModel):
    _name = 'project.agency'

    partner_id = fields.Many2one('res.partner', string='Responsible Agency', track_visibility='onchange', domain=[('is_portal', '=',False)])
    email = fields.Char(string='Email', related='partner_id.email', track_visibility='onchange', readonly=True)
    mobile = fields.Char(string='Mobile',related='partner_id.mobile', track_visibility='onchange',readonly=True)

    @api.multi
    def create_assign_partner(self):
        partners = self.env['project.issue'].browse(self._context.get('active_id'))
        partners.agency_contect = self.partner_id.id
        partners.agency_email = self.partner_id.email
        partners.agency_mobile = self.partner_id.mobile