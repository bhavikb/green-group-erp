# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Key Concepts IT Services LLP.
#    (<http://keyconcepts.co.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

{
    "name": "Project Booking",
    "version": "1.0",
    'author': 'Key Concepts IT Services LLP.',
    'website': 'http://keyconcepts.co.in',
    "category": "Booking",
    "description": """
        ..
    """,
    "depends": ['project', 'document', 'crm', 'project_issue', 'sale_service', 'base_setup', 'mail', 'web', 'website'],
    "data": [
        "wizard/kc_crm_create_user_wizard.xml",
        "wizard/kc_task_assign_ageny_wiz.xml",
        #"views/my_portal_view.xml",
        "views/kc_crm_view.xml",
        "views/kc_sale_view.xml",
        "views/kc_project_view.xml",
        "views/kc_auth_sign_up_data.xml",
        ],
    "auto_install": False,
    "installable": True,
}
