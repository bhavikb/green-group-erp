# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Key Concepts IT Services LLP.
#    (<http://keyconcepts.co.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from openerp import models, fields, api, _
from openerp.exceptions import ValidationError
import base64
import csv


class CRMLeadProject(models.TransientModel):

    """ Customer Lead Find from project """
    _name = 'find.crm.project.lead'

    order_line_file = fields.Binary('Contact list File')
    datas_fname = fields.Char('File Name')
    is_import = fields.Boolean('File imported?')
    search_option = fields.Selection([('mobile', 'Mobile'), ('name', 'Name')], string="Search By", default='mobile')

    @api.multi
    def import_order_lines(self):
        # only for csv file
        cnt = 0
        lst = []
        crm_lead_number = []
        crm_lead_name = []
        crm_lead_list = []
        if not self.order_line_file:
            raise ValidationError(_('Please select file to import.'))
        reader = base64.decodestring(self.order_line_file)
        file_reader = csv.reader(reader.split('\n'), delimiter=',')
        for row in file_reader:
            c_list = {}
            if row:
                if cnt == 0:
                    for i in range(0, len(row)):
                        lst.append(row[i].lower())
                if cnt > 0:
                    for i in range(0, len(row)):
                        c_list[lst[i]] = row[i]
                        if c_list.get('mobile'):
                            crm_lead_number.append(c_list.get('mobile'))
                        if c_list.get('name'):
                            crm_lead_name.append(c_list.get('name'))
                cnt += 1
        if self.search_option == 'mobile':
            for number in crm_lead_number:
                lead_obj = ""
                lead_obj = self.env['crm.lead'].search([('mobile', 'ilike', number)])
                if lead_obj:
                    for lead_number in lead_obj:
                        if lead_number.id not in crm_lead_list:
                            crm_lead_list.append(lead_number.id)
                lead_obj = ""
                lead_obj = self.env['crm.lead'].search([('phone', 'ilike', number)])
                if lead_obj:
                    for lead_number in lead_obj:
                        if lead_number.id not in crm_lead_list:
                            crm_lead_list.append(lead_number.id)
        else:
            for name in crm_lead_name:
                lead_obj = self.env['crm.lead'].search([('name', 'ilike', name)])
                if lead_obj:
                    for lead_name in lead_obj:
                        if lead_name.id not in crm_lead_list:
                            crm_lead_list.append(lead_name.id)
                lead_obj = self.env['crm.lead'].search([('partner_id.name', 'ilike', name)])
                if lead_obj:
                    for lead_name in lead_obj:
                        if lead_name.id not in crm_lead_list:
                            crm_lead_list.append(lead_name.id)
        crm_lead_ids = crm_lead_list
        imd = self.env['ir.model.data']
        action = imd.xmlid_to_object('crm.crm_lead_opportunities_tree_view')
        tree_view_id = imd.xmlid_to_res_id('crm.crm_case_tree_view_leads')
        form_view_id = imd.xmlid_to_res_id('crm.crm_case_form_view_leads')
        kanbanview_id = imd.xmlid_to_res_id('crm.crm_case_kanban_view_leads')
        result = {
            'name': action.name,
            'help': action.help,
            'type': action.type,
            'views': [[kanbanview_id, 'kanban'], [form_view_id, 'form'], [tree_view_id, 'tree']],
            'target': action.target,
            'context': action.context,
            'res_model': action.res_model,
        }
        if len(crm_lead_ids) >= 1:
            result['domain'] = "[('id','in', %s)]" % crm_lead_ids
        else:
            result = {'type': 'ir.actions.act_window_close'}
        return result
