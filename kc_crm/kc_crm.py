from openerp import models, fields, api, _
from openerp.exceptions import UserError

class Partner(models.Model):
    _inherit = "res.partner"

    brokres = fields.Boolean(string='Is a Broker')
    lead = fields.Boolean(string='Is a Lead')

class Department(models.Model):
    _inherit = "hr.department"
    _order = "priority asc, name asc"

    priority = fields.Integer(string='Sequence')

class CrmLead(models.Model):
    _inherit = "crm.lead"

    mobile = fields.Char(string='Secondary Mobile')
    mobile2 = fields.Char(related="partner_id.mobile", string='Mobile')
    broker_id = fields.Many2one('res.partner', string='Broker', select=True, track_visibility='onchange', domain=[('brokres', '=', True)])
    unit_type = fields.Many2one('product.unit', string='Unit Type')
    budget_id = fields.Many2one('product.budget', string='Budget')
    priority = fields.Selection([('0', 'Normal'), ('1', 'Hot'), ('2', 'Warm'), ('3', 'Cold')], 'Rating', select=True)

    @api.one
    def action_set_won(self):
        res = super(CrmLead, self).action_set_won()
        self.partner_id.customer = True
        self.partner_id.lead = False
        return res

    @api.model
    def create(self, vals):
        res = super(CrmLead, self).create(vals)
        res.partner_id.lead = True
        return res

    @api.multi
    def on_change_partner_id(self, partner_id):
        res = super(CrmLead, self).on_change_partner_id(partner_id)
        if partner_id:
            res['value']['mobile2'] = res['value']['mobile']
            return res
        
class UnitType(models.Model):
    _name = "product.unit"

    name = fields.Char(string='Unit Name')

class BudgetType(models.Model):
    _name = "product.budget"

    name = fields.Char(string='Budget')